FROM nginx:alpine
COPY ./dist/cabinet-sigma /usr/share/nginx/html
EXPOSE 80 443
ENTRYPOINT [ "nginx", "-g", "daemon off;" ]
