import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Store, Section } from '@app/store';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  isAuthenticated: boolean;

  constructor(private router: Router, private store: Store) {
    this.store.select(Section.user).subscribe(user => (this.isAuthenticated = !!user));
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.isAuthenticated) {
      return true;
    }
    // if (localStorage.getItem('currentUser')) {
    //     // logged in so return true
    //     return true;
    // }

    // not logged in so redirect to login page with the return url
    this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
    return false;
  }
}
