import {
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpEvent,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AuthenticationService } from '@services';

@Injectable()
export class JwtResponseInterceptor implements HttpInterceptor {
  constructor(private authService: AuthenticationService) {}
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      tap(evt => {
        if (evt instanceof HttpResponse) {
          if (evt.headers && evt.headers.get('X-Authorization')) {
            const token = evt.headers.get('X-Authorization');
            if (!!token) {
              this.authService.refreshToken(token);
              // console.log('Updated Token:::', token);
            }
          }
        }
      })
    );
  }
}
