import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // add authorization header with jwt token if available
    const currentUser = JSON.parse(localStorage.getItem('cabinetCurrentUser'));
    const regular = /eqvola-sigma.cloudapp.net/gi;
    const regular2 = /eqvolasigmablob.blob.core.windows.net/gi;

    const isBaseUrl =
      request.url.search(regular) !== -1 || request.url.search(regular2) !== -1;

    if (currentUser && currentUser.token && isBaseUrl) {
      // console.log('INTERCEPTOR ADDED TOKIN:::', request.url);
      request = request.clone({
        setHeaders: { Authorization: `${currentUser.token}` }
      });
    } else {
      // console.log('WITHOUT TOKIN:::', request.url);
    }

    return next.handle(request);
  }
}
