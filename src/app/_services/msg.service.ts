import { Injectable } from '@angular/core';
import { MessageService } from 'primeng/api';

@Injectable({ providedIn: 'root' })
export class MsgService extends MessageService {
  constructor(private messageService: MessageService) {
    super();
  }

  clearMessages(key = 'app') {
    this.messageService.clear(key);
  }

  showWarnMsg(summary, detail = 'Warning', life = 3000, key = 'app') {
    this.messageService.add({
      key,
      severity: 'warn',
      summary,
      detail,
      life
    });
  }

  showSuccessMsg(
    detail = 'Your data successfully changed!',
    summary = 'Success Message',
    key = 'app'
  ) {
    this.messageService.add({
      key: key,
      severity: 'success',
      summary: summary,
      detail: detail,
      life: 2000
    });
  }

  showErrorMsg(
    detail = 'Something went wrong, please try again',
    summary = 'Error Message',
    key = 'app'
  ) {
    this.messageService.add({
      key: key,
      severity: 'error',
      summary: summary,
      detail: detail,
      life: 2500
    });
  }

  // Handle Error Message from BackEnd
  // Change password errors
  changePassErrors(message: string) {
    switch (message) {
      case '': {
        // Incorrect password
        this.showErrorMsg('Incorrect Password');
        break;
      }
      case 'Reset code cannot be updated': {
        // Please try again after 60 secons
        this.showErrorMsg('Please try again after 60 seconds');
        break;
      }
      case 'Failed to change password': {
        // console.log('Incorrect SMS code');
        this.showErrorMsg('Incorrect SMS code');
        break;
      }
      default: {
        this.showErrorMsg();
      }
    }
  }

  showLoginError(status: number) {
    switch (status) {
      case 0: {
        // Incorrect password
        this.showErrorMsg('Email or password are incorrect');
        break;
      }
      case 5: {
        // Incorrect TFA code
        this.showErrorMsg('Wrong two-factor authentication code');
        break;
      }
      default: {
        this.showErrorMsg();
      }
    }
  }

  showForgotError(message: string) {
    switch (message) {
      case 'Wrong email or phone number': {
        // Incorrect data
        this.showErrorMsg('Wrong email or phone number');
        break;
      }
      case 'code error': {
        // Incorrect code
        this.showErrorMsg('Invalid confirmation code');
        break;
      }
      default: {
        this.showErrorMsg();
      }
    }
  }

  showRegistrationError(message: string) {
    switch (message) {
      case 'Wrong code': {
        // Incorrect code
        this.showErrorMsg('Wrong code');
        break;
      }
      case 'Phone number already exist': {
        // Phone number already exist
        this.showErrorMsg('Phone number already exist');
        break;
      }
      case 'ExistenEmail': {
        // Email already exist
        this.showErrorMsg('Email already exist');
        break;
      }
      default: {
        this.showErrorMsg();
      }
    }
  }

  showCreateOrderError(message: string) {
    switch (message) {
      case 'Wrong authorization': {
        // Wrong authorization
        this.showErrorMsg('Wrong authorization');
        break;
      }

      case 'UnexpectedAmount': {
        // Unexpected Amount
        this.showErrorMsg('Amount should be greater than 0');
        break;
      }

      case 'UnexpectedCurrency': {
        // Unexpected Currency
        this.showErrorMsg('Unexpected Currency');
        break;
      }

      case 'UnexpectedPrice': {
        // Unexpected Price
        this.showErrorMsg('Price should be greater than 0');
        break;
      }

      case 'NonExistenOrder': {
        // Non Existen Order
        this.showErrorMsg('Non Existen Order');
        break;
      }

      case 'ExpiredOrder': {
        // Expired Order
        this.showErrorMsg('Expired Order');
        break;
      }

      case 'UnexpectedOrderType': {
        // Unexpected Order Type (1 - Buy, 2 - Sell)
        this.showErrorMsg('Unexpected Order Type');
        break;
      }

      case 'requiredFields': {
        // not all fields are filled
        this.showErrorMsg('Not all fields are filled');
        break;
      }

      case 'takeProfitLessPrice': {
        this.showErrorMsg('Take Profit cannot be less than Price');
        break;
      }

      case 'takeProfitGreatePrice': {
        this.showErrorMsg('Take Profit cannot be greate than Price');
        break;
      }

      case 'stopLossGreatePrice': {
        // not all fields are filled
        this.showErrorMsg('Stop Loss cannot be greate than Price');
        break;
      }

      case 'stopLossLessPrice': {
        // not all fields are filled
        this.showErrorMsg('Stop Loss cannot be less than Price');
        break;
      }

      default: {
        this.showErrorMsg();
      }
    }
  }

  showProfileError(message: string) {
    switch (message) {
      case 'MessengerNameMinLength': {
        // Incorrect code
        this.showErrorMsg('Messenger name should be at least 4 characters');
        break;
      }

      default: {
        this.showErrorMsg();
      }
    }
  }

  showWalletMsg(message: string) {
    switch (message) {
      case 'success': {
        // Incorrect code
        this.showSuccessMsg('Your request has been sent!');
        break;
      }
      // case 'MessengerNameMinLength': {
      //   // Incorrect code
      //   this.showErrorMsg('Messenger name should be at least 4 characters');
      //   break;
      // }

      default: {
        this.showErrorMsg();
      }
    }
  }

  showSupportMsg(message: any) {
    const msg = typeof message === 'string' ? message : message.message;
    switch (msg) {
      case 'limitFileSize': {
        // Incorrect code
        this.showErrorMsg(
          message.fileName + ': should be less than ' + message.limits + 'Mb'
        );
        break;
      }

      case 'limitTypes': {
        // Incorrect code
        this.showErrorMsg(
          message.fileName +
            ': has not supported types. You can upload: ' +
            message.limits
        );
        break;
      }

      case 'uploadingFileStart': {
        this.showWarnMsg(
          "Please don't close Tab!",
          'Attachments Uploading...',
          25000
        );
        break;
      }

      case 'success': {
        this.clearMessages();
        this.showSuccessMsg('Your request successfully sent');
        break;
      }

      default: {
        this.showErrorMsg();
      }
    }
  }

  showMyOrdersMsg(message: any) {
    switch (message) {
      case 'successfullyDelete': {
        // Incorrect code
        this.showSuccessMsg('Order was succesfully canceled');
        break;
      }

      case 'successfullyUpdated': {
        // Incorrect code
        this.showSuccessMsg('Order was succesfully updated');
        break;
      }

      default: {
        this.showErrorMsg(message);
      }
    }
  }
}
