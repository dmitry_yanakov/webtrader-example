import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Store, Section } from '@app/store';

export interface TwoFactorAction {
  name: string;
  status: number;
  options: TwoFactorActionOption[];
}

interface TwoFactorActionOption {
  key: string;
}

export interface TwoFactorMethods {
  type: string;
  status: number;
}
export interface TwoFactorSettings {
  type: string;
  action: string;
  state: number;
  options: TwoFactorSettingsOption[];
}

interface TwoFactorSettingsOption {
  key: string;
  value: number;
}

@Injectable({
  providedIn: 'root'
})
export class TwoFactorService {
  public static TwoFactors = [];

  private baseUrl = environment.baseUrl;

  constructor(private http: HttpClient, private store: Store) {}

  getAllTwoFactor(): Observable<TwoFactorMethods[]> {
    return this.http.get<TwoFactorMethods[]>(
      `${this.baseUrl}/twofactor/getAllTwoFactor`
    );
  }

  updateTwoFactor(type: string, status: number) {
    return this.http.put(`${this.baseUrl}/twofactor/updateTwoFactor`, {
      type,
      status
    });
  }

  getAllActions(): Observable<TwoFactorAction[]> {
    return this.http.get<TwoFactorAction[]>(
      `${this.baseUrl}/twofactor/getAllActions`
    );
  }

  updateAction(type: string, status: number) {
    return this.http.put(`${this.baseUrl}/twofactor/updateAction`, {
      type,
      status
    });
  }

  getUserSettings(): Observable<TwoFactorSettings[]> {
    return this.http.get<TwoFactorSettings[]>(
      `${this.baseUrl}/twofactor/getUserSettings/${this.userEmail}/`
    );
  }

  createSettings(parameters: any) {
    return this.http.post(
      `${this.baseUrl}/twofactor/createSettings`,
      parameters
    );
  }

  updateSettings(parameters: any) {
    return this.http.put(
      `${this.baseUrl}/twofactor/updateSettings`,
      parameters
    );
  }

  private get userEmail() {
    return this.store.value[Section.user].email;
  }
}
