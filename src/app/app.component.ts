import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ApiService } from '@services';
import * as jwtDecode from 'jwt-decode';
import { Store, Section } from './store';
import { PoloniexService } from './shared/socket/poloniex.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private translate: TranslateService,
    private store: Store,
    private poloniex: PoloniexService
  ) {
    this.initUser();
    this.initTranslate();
    this.poloniex.tickerData$.subscribe();
  }

  initUser() {
    // if we have token in local storage - init user
    const token = localStorage.getItem('cabinetCurrentUser');
    if (token) {
      const userDecoded = jwtDecode(token);
      this.store.set(Section.user, userDecoded);
      // console.log(this.store.value);
    }
  }

  initTranslate() {
    // init available languages
    this.translate.addLangs(ApiService.TranslateLangs);

    // this language will be used as a fallback when a translation isn't found in the current language
    this.translate.setDefaultLang('en');

    // the lang to use, if the lang isn't available, it will use the current loader to get them
    const browserLang = this.translate.getBrowserLang();
    // if we have translate for browser language we use it otherwise we use 'en'
    const hasTranslate = ApiService.TranslateLangs.includes(browserLang);
    this.translate.use(hasTranslate ? browserLang : 'en');
  }
}
