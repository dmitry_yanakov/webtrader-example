import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import {
  HttpClientModule,
  HTTP_INTERCEPTORS,
  HttpClient
} from '@angular/common/http';

import { AppComponent } from './app.component';

import { JwtInterceptor, ErrorInterceptor } from './_helpers';

import { MDBBootstrapModule } from 'angular-bootstrap-md';

import { Routes, RouterModule, Router } from '@angular/router';
import { AuthenticationService, DocumentService } from '@services';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AuthGuard } from './_guards';

import { DeviceDetectorModule } from 'ngx-device-detector';
import { DeviceDetectorService } from 'ngx-device-detector';

import { Store } from './store';
import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/api';
import {
  SignalRConfiguration,
  SignalRModule,
  ConnectionTransport,
  ConnectionTransports
} from 'ng2-signalr';
import { JwtResponseInterceptor } from './_helpers/jwt-response.interseptor';

const appRoutes: Routes = [
  { path: '', loadChildren: './pc-version/pc-version.module#PcVersionModule' },
  { path: '**', redirectTo: '' },
  { path: '', loadChildren: './mobile/mobile.module#MobileModule' },
  { path: '**', redirectTo: '' }
];

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

export function createConfig(): SignalRConfiguration {
  const c = new SignalRConfiguration();
  // c.hubName = 'quotetionsHub';
  // c.hubName = 'mainHub';
  // c.qs = { user: 'viktorpar@gmail.com' };
  // c.url = 'http://eqvola-sigma.cloudapp.net:8082';
  // c.logging = true;

  // c.jsonp = true;

  return c;
}

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    MDBBootstrapModule.forRoot(),
    DeviceDetectorModule.forRoot(),
    SignalRModule.forRoot(createConfig),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    ToastModule
  ],
  declarations: [AppComponent],
  providers: [
    Store,
    AuthenticationService,
    MessageService,
    DocumentService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtResponseInterceptor,
      multi: true
    },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(
    private router: Router,
    private deviceDetector: DeviceDetectorService
  ) {
    const isMobile = this.deviceDetector.isMobile();
    const isTablet = this.deviceDetector.isTablet();

    console.log(deviceDetector);

    if (isMobile) {
      this.router.resetConfig([
        { path: '', loadChildren: './mobile/mobile.module#MobileModule' },
        { path: '**', redirectTo: '' }
      ]);
    }
  }
}
