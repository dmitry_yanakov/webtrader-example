import { Component, OnInit, OnDestroy } from '@angular/core';
import {
  widget,
  IChartingLibraryWidget,
  ChartingLibraryWidgetOptions,
  LanguageCode,
  ContextMenuItem
} from '../../../assets/charting_library/charting_library.min';

import { ChartService } from './chart.service';

import { Store, Section } from '@app/store';
import { filter } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { CurrencyPair } from '@services';

@Component({
  selector: 'app-chart-trading-view',
  templateUrl: './chart-trading-view.component.html',
  styleUrls: ['./chart-trading-view.component.scss'],
  providers: [ChartService]
})
export class ChartTradingViewComponent implements OnInit, OnDestroy {
  private symbol = '';
  private interval = '15';
  private containerId = 'tv_chart_container';
  private chartsStorageUrl = 'https://saveload.tradingview.com';
  private chartsStorageApiVersion: ChartingLibraryWidgetOptions['charts_storage_api_version'] =
    '1.1';
  private clientId = 'tradingview.com';
  private userId = 'public_user_id';
  private fullscreen = false;
  private autosize = true;
  private studiesOverrides = {};
  private tvWidget: IChartingLibraryWidget | null = null;
  private libraryPath: ChartingLibraryWidgetOptions['library_path'] =
    '/assets/charting_library/';

  private sub: Subscription;

  private isChartReady = false;

  constructor(private chartService: ChartService, private store: Store) {}

  ngOnInit() {
    this.symbol = this.normalizePair(this.store.value.currencyPairSelected);
    this.initChangePair();
    this.initChart();
  }

  initChart() {
    const widgetOptions: ChartingLibraryWidgetOptions = {
      debug: false,
      symbol: this.symbol,
      datafeed: this.chartService.getChart(),
      interval: this.interval,
      container_id: this.containerId,
      library_path: this.libraryPath,
      locale: this.getLanguageFromURL() || 'en',
      disabled_features: [
        'use_localstorage_for_settings',
        'header_symbol_search',
        'header_compare',
        'compare_symbol',
        'context_menus',
        'header_saveload',
        'go_to_date'
      ],
      enabled_features: ['study_templates'],
      charts_storage_url: this.chartsStorageUrl,
      charts_storage_api_version: this.chartsStorageApiVersion,
      client_id: this.clientId,
      user_id: this.userId,
      fullscreen: this.fullscreen,
      autosize: this.autosize,
      studies_overrides: this.studiesOverrides,
      overrides: {
        // "mainSeriesProperties.showCountdown": true,
        'paneProperties.background': '#f9f9f9',
        'paneProperties.vertGridProperties.color': '#e5e5e5',
        'paneProperties.horzGridProperties.color': '#e5e5e5',
        'symbolWatermarkProperties.transparency': 100,
        'scalesProperties.textColor': '#000',
        'mainSeriesProperties.candleStyle.wickUpColor': '#336854',
        'mainSeriesProperties.candleStyle.wickDownColor': '#7f323f'
      },
      custom_css_url: '/assets/customTV.css',
      time_frames: [

        { text: "3y", resolution: "D", description: "All history", title: "All" },
        { text: "1y", resolution: "60", description: "last year", title: "Y" },
        { text: "3m", resolution: "60", description: "last 3 months", title: "3M" },
        { text: "1m", resolution: "15", description: "last month", title: "1M" },
        { text: "7d", resolution: "15", description: "last week", title: "7D" },
        { text: "3d", resolution: "15", description: "last 3 days", title: "3D" },
        { text: "1d", resolution: "5", description: "last day", title: "1D" },
        { text: "6h", resolution: "1", description: "last 6 hours", title: "6h" }
//        { text: "50y", resolution: "6M", description: "50 Years" },
//        { text: "3y", resolution: "W", description: "3 Years", title: "3yr" },
//        { text: "8m", resolution: "D", description: "8 Month" },
//        { text: "3d", resolution: "5", description: "3 Days" },
//        { text: "1000y", resolution: "W", description: "All", title: "All" },
    ]
    };

    const tvWidget = new widget(widgetOptions);
    this.tvWidget = tvWidget;

    tvWidget.onChartReady(() => {
      this.isChartReady = true;
    });
  }

  getLanguageFromURL(): LanguageCode | null {
    const regex = new RegExp('[\\?&]lang=([^&#]*)');
    const results = regex.exec(location.search);

    return results === null
      ? null
      : (decodeURIComponent(results[1].replace(/\+/g, ' ')) as LanguageCode); // leave this functions and get Lang param from i18 lib.
  }

  private initChangePair() {
    this.sub = this.store
      .select(Section.currencyPairSelected)
      .pipe(filter(Boolean))
      .subscribe(pair => {
        if (this.isChartReady) {
          this.changePair(pair);
        }
      });
  }

  private normalizePair(pair: CurrencyPair) {
    return 'Poloniex:' + pair.targetCurrency + '/' + pair.currency;
    //return pair.targetCurrency + '/' + pair.currency;
  }

  changePair(pair) {
    const pairExchange = this.normalizePair(pair);

    this.tvWidget.setSymbol(pairExchange, '15', () => {
      console.log('CHART Pair has changed');
    });
  }

  pushChart() {
    this.tvWidget.applyOverrides({ 'paneProperties.background': 'black' });
  }

  createOrderLine() {
    this.tvWidget
      .chart()
      .createOrderLine({ disableUndo: false })
      .onMove(function() {
        this.setText('onMove called');
      })
      .onModify('onModify called', function(text) {
        this.setText(text);
        // here we can show pop up over chart where user can change Order quantity
      })
      .onCancel('onCancel called', function(text) {
        this.remove();
      })
      .setText('TAKE PROFIT: 73.5 (5,64%)')
      .setQuantity('2')
      .setPrice(3500.0)
      .setExtendLeft(true)
      .setLineStyle(0)
      .setLineLength(25)
      .setBodyBackgroundColor('#00ff00');

    // this.tvWidget.chart().createExecutionShape({})
    //   .setText("@1,320.75 Limit Buy 1")
    //   .setTooltip("@1,320.75 Limit Buy 1")
    //   .setTextColor("rgba(0,255,0,0.5)")
    //   .setArrowColor("#0F0")
    //   .setDirection("buy")
    //   .setPrice(15.5);
  }

  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe();
    }

    if (this.tvWidget !== null) {
      this.tvWidget.remove();
      this.tvWidget = null;
    }
  }
}
