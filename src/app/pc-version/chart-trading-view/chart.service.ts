import { Injectable } from '@angular/core';
import fetch from 'isomorphic-fetch';

import io from 'socket.io-client';
const socket_url = 'wss://streamer.cryptocompare.com';
const socket = io(socket_url);

const supportedResolutions = [
  '1',
  '3',
  '5',
  '15',
  '30',
  '60',
  '120',
  '240',
  'D'
];

const config = {
  supported_resolutions: supportedResolutions,
  symbols_types: [
    { name: 'All types', value: '' },
    { name: 'Stock', value: 'stock' },
    { name: 'Index', value: 'index' }
  ]
};

@Injectable({
  providedIn: 'root'
})
export class ChartService {
  api_root = 'https://min-api.cryptocompare.com';
  history = {};
  _subs = [];

  constructor() {}

  getChart() {
    const self = this;
    return {
      onReady: cb => {
        //console.log('=====onReady running');
        setTimeout(() => cb(config), 0);
      },
      searchSymbols: (
        userInput,
        exchange,
        symbolType,
        onResultReadyCallback
      ) => {
        //console.log('====Search Symbols running');

        let result = [
          {
            symbol: 'MySymbol',
            full_name: 'MySymboltatatat', // e.g. BTCE:BTCUSD
            description: 'MySymboltatatatadawdawdawd',
            exchange: 'Xetra',
            ticker: 'MSB',
            type: 'stock' // or "futures" or "bitcoin" or "forex" or "index"
          },
          {
            symbol: '1MySymbol',
            full_name: '1MySymboltatatat', // e.g. BTCE:BTCUSD
            description: '1MySymboltatatatadawdawdawd',
            exchange: '1Xetra',
            ticker: '1MSB',
            type: 'index' // or "futures" or "bitcoin" or "forex" or "index"
          },
          {
            symbol: '2MySymbol',
            full_name: '2MySymboltatatat', // e.g. BTCE:BTCUSD
            description: '2MySymboltatatatadawdawdawd',
            exchange: '2Xetra',
            ticker: '2MSB',
            type: 'stock' // or "futures" or "bitcoin" or "forex" or "index"
          }
        ];

        result = result.filter(item => item.type === symbolType);

        setTimeout(() => {
          onResultReadyCallback(result);
        }, 0);
      },
      resolveSymbol: (
        symbolName,
        onSymbolResolvedCallback,
        onResolveErrorCallback
      ) => {
        // expects a symbolInfo object in response
        //console.log('======resolveSymbol running');
        // console.log('resolveSymbol:',{symbolName})
        //symbolName = 'Poloniex:' + symbolName;
        const split_data = symbolName.split(/[:/]/);
        console.log(split_data, 'SPLIT DATA');

        const symbol_stub = {
          name: symbolName,
          description: '',
          type: 'crypto',
          session: '24x7',
          timezone: 'Etc/UTC',
          ticker: symbolName,
          //exchange: split_data[0],
          minmov: 1,
          pricescale: 100000000,
          has_intraday: true,
          intraday_multipliers: ['1', '60'],
          supported_resolution: supportedResolutions,
          volume_precision: 8,
          data_status: 'streaming'
        };

        if (split_data[2].match(/USD|EUR|JPY|AUD|GBP|KRW|CNY|USDT/)) {
          symbol_stub.pricescale = 10000;
        }
        setTimeout(function() {
          onSymbolResolvedCallback(symbol_stub);
          // console.log('Resolving that symbol....', symbol_stub);
        }, 0);

        // onResolveErrorCallback('Not feeling it today')
      },
      getBars: (
        symbolInfo,
        resolution,
        from,
        to,
        onHistoryCallback,
        onErrorCallback,
        firstDataRequest
      ) => {
        // console.log('FROM::', from);
        // console.log('TO::', to);
        //console.log('=====getBars running', this);
        // console.log('function args',arguments)
        // console.log(`Requesting bars between ${new Date(from * 1000).toISOString()} and ${new Date(to * 1000).toISOString()}`)
        this.getBars2(symbolInfo, resolution, from, to, firstDataRequest, 2000)
          .then(bars => {
            if (bars.length) {
              onHistoryCallback(bars, { noData: false });
            } else {
              onHistoryCallback(bars, { noData: true });
            }
          })
          .catch(err => {
            console.log({ err });
            onErrorCallback(err);
          });
      },
      subscribeBars: (
        symbolInfo,
        resolution,
        onRealtimeCallback,
        subscribeUID,
        onResetCacheNeededCallback
      ) => {
        //console.log('=====subscribeBars runnning');
        this.subscribeBars(
          symbolInfo,
          resolution,
          onRealtimeCallback,
          subscribeUID,
          onResetCacheNeededCallback
        );
      },
      unsubscribeBars: subscriberUID => {
        //console.log('=====unsubscribeBars running');
        this.unsubscribeBars(subscriberUID);
      },
      calculateHistoryDepth: (resolution, resolutionBack, intervalBack) => {
        //optional
        //console.log('=====calculateHistoryDepth running');
        // while optional, this makes sure we request 24 hours of minute data at a time
        // CryptoCompare's minute data endpoint will throw an error if we request data beyond 7 days in the past, and return no data
        return resolution < 60
          ? { resolutionBack: 'D', intervalBack: '1' }
          : undefined;
      },
      getMarks: (
        symbolInfo,
        startDate,
        endDate,
        onDataCallback,
        resolution
      ) => {
        //optional
        //console.log('=====getMarks running');
      },
      getTimeScaleMarks: (
        symbolInfo,
        startDate,
        endDate,
        onDataCallback,
        resolution
      ) => {
        //optional
        //console.log('=====getTimeScaleMarks running');
      },
      getServerTime: cb => {
        //console.log('=====getServerTime running');
      }
    };
  }

  getBars2(symbolInfo, resolution, from, to, first, limit) {
    //console.log('Get Barst Works');
    //symbolInfo.name = 'Poloniex:' + symbolInfo.name;
    const split_symbol = symbolInfo.name.split(/[:/]/);
    const url =
      resolution === 'D'
        ? '/data/histoday'
        : resolution >= 60
        ? '/data/histohour'
        : '/data/histominute';
    const qs = {
      e: split_symbol[0],
      fsym: split_symbol[1],
      tsym: split_symbol[2],
      toTs: to ? to : '',
      limit: limit ? limit : 2000
      // aggregate: 1//resolution
    };
    // console.log({qs})

    // console.log(
    //   `${this.api_root}${url}?e=${qs.e}&fsym=${qs.fsym}&tsym=${qs.tsym}&toTs=${
    //     qs.toTs
    //   }&limit=${limit}`
    // );
    const urlString = `${this.api_root}${url}?e=${qs.e}&fsym=${qs.fsym}&tsym=${
      qs.tsym
    }&toTs=${qs.toTs}&limit=${limit}`;

    // console.log('URL:::', urlString);

    return fetch(urlString, {
      method: 'GET'
    })
      .then(data => {
        return data.json();
      })
      .then(data => {
        // console.log('DATA', data);

        if (data.Response && data.Response === 'Error') {
          // console.log('CryptoCompare API error:', data.Message);
          return [];
        }
        if (data.Data.length) {
          // console.log(
          //   `Actually returned: ${new Date(
          //     data.TimeFrom * 1000
          //   ).toISOString()} - ${new Date(data.TimeTo * 1000).toISOString()}`
          // );
          const bars = data.Data.map(el => {
            return {
              time: el.time * 1000, //TradingView requires bar time in ms
              low: el.low,
              high: el.high,
              open: el.open,
              close: el.close,
              volume: el.volumefrom
            };
          });
          if (first) {
            const lastBar = bars[bars.length - 1];

            this.history[symbolInfo.name] = { lastBar: lastBar };
          }
          return bars;
        } else {
          return [];
        }
      });
  }

  subscribeBars(symbolInfo, resolution, updateCb, uid, resetCache) {
    this.initSockets();
    const channelString = createChannelString(symbolInfo);
    socket.emit('SubAdd', { subs: [channelString] });

    const newSub = {
      channelString,
      uid,
      resolution,
      symbolInfo,
      lastBar: this.history[symbolInfo.name].lastBar,
      listener: updateCb
    };

    //console.log('UID------', uid);

    this._subs.push(newSub);
  }

  unsubscribeBars(uid) {
    const subIndex = this._subs.findIndex(e => e.uid === uid);
    if (subIndex === -1) {
      //console.log("No subscription found for ",uid)
      return;
    }
    const sub = this._subs[subIndex];
    socket.emit('SubRemove', { subs: [sub.channelString] });
    this._subs.splice(subIndex, 1);
  }

  initSockets() {
    socket.on('connect', () => {
      //console.log('===Socket connected');
    });
    socket.on('disconnect', e => {
      //console.log('===Socket disconnected:', e);
    });
    socket.on('error', err => {
      //console.log('====socket error', err);
    });
    socket.on('m', e => {
      //console.log('Message----', e);

      // Тут нам потрібно просто получити данні з Poloniex і розпарсити їх правильно

      // here we get all events the CryptoCompare connection has subscribed to
      // we need to send this new data to our subscribed charts
      const _data = e.split('~');
      if (_data[0] === '3') {
        // console.log('Websocket Snapshot load event complete')
        return;
      }
      const data = {
        sub_type: parseInt(_data[0], 10),
        exchange: _data[1],
        to_sym: _data[2],
        from_sym: _data[3],
        trade_id: _data[5],
        ts: parseInt(_data[6], 10),
        volume: parseFloat(_data[7]),
        price: parseFloat(_data[8])
      };

      const channelString = `${data.sub_type}~${data.exchange}~${data.to_sym}~${
        data.from_sym
      }`;

      const sub = this._subs.find(item => item.channelString === channelString);

      if (sub) {
        // disregard the initial catchup snapshot of trades for already closed candles
        if (data.ts < sub.lastBar.time / 1000) {
          return;
        }

        const _lastBar = updateBar(data, sub);

        // send the most recent bar back to TV's realtimeUpdate callback
        sub.listener(_lastBar);
        // update our own record of lastBar
        sub.lastBar = _lastBar;
      }
    });
  }
}

// Take a single trade, and subscription record, return updated bar
function updateBar(data, sub) {
  const lastBar = sub.lastBar;
  let resolution = sub.resolution;
  if (resolution.includes('D')) {
    // 1 day in minutes === 1440
    resolution = 1440;
  } else if (resolution.includes('W')) {
    // 1 week in minutes === 10080
    resolution = 10080;
  }
  const coeff = resolution * 60;
  // console.log({coeff})
  const rounded = Math.floor(data.ts / coeff) * coeff;
  const lastBarSec = lastBar.time / 1000;
  let _lastBar;

  if (rounded > lastBarSec) {
    // create a new candle, use last close as open **PERSONAL CHOICE**
    _lastBar = {
      time: rounded * 1000,
      open: lastBar.close,
      high: lastBar.close,
      low: lastBar.close,
      close: data.price,
      volume: data.volume
    };
  } else {
    // update lastBar candle!
    if (data.price < lastBar.low) {
      lastBar.low = data.price;
    } else if (data.price > lastBar.high) {
      lastBar.high = data.price;
    }

    lastBar.volume += data.volume;
    lastBar.close = data.price;
    _lastBar = lastBar;
  }
  return _lastBar;
}

// takes symbolInfo object as input and creates the subscription string to send to CryptoCompare
function createChannelString(symbolInfo) {
  const channel = symbolInfo.name.split(/[:/]/);
  const exchange = channel[0] === 'GDAX' ? 'Coinbase' : channel[0];
  const to = channel[2];
  const from = channel[1];
  // subscribe to the CryptoCompare trade channel for the pair and exchange
  return `0~${exchange}~${from}~${to}`;
}
