import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LineOfCurrencyQuotationsComponent } from './line-of-currency-quotations.component';

describe('LineOfCurrencyQuotationsComponent', () => {
  let component: LineOfCurrencyQuotationsComponent;
  let fixture: ComponentFixture<LineOfCurrencyQuotationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LineOfCurrencyQuotationsComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LineOfCurrencyQuotationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
