import {
  Component,
  OnInit,
  OnDestroy,
  Input,
  ChangeDetectionStrategy
} from '@angular/core';

@Component({
  selector: 'app-line-of-currency-quotations',
  templateUrl: './line-of-currency-quotations.component.html',
  styleUrls: ['./line-of-currency-quotations.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LineOfCurrencyQuotationsComponent implements OnInit, OnDestroy {
  constructor() {}

  @Input() pairsInfo = [];

  ngOnInit() {}

  ngOnDestroy() {}
}
