import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LineOfCurrencyComponent } from './line-of-currency.component';

describe('LineOfCurrencyComponent', () => {
  let component: LineOfCurrencyComponent;
  let fixture: ComponentFixture<LineOfCurrencyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LineOfCurrencyComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LineOfCurrencyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
