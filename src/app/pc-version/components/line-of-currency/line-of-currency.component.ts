import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserProfile } from '@models';
import { Store, Section } from '@app/store';
import { TranslateService } from '@ngx-translate/core';
import { SignalRService } from '@app/_services/signal-r.service';

@Component({
  selector: 'app-line-of-currency',
  templateUrl: './line-of-currency.component.html',
  styleUrls: ['./line-of-currency.component.scss']
})
export class LineOfCurrencyComponent implements OnInit, OnDestroy {
  showOptions = false;
  translateLangs: string[] = [];
  user: UserProfile;
  quotationsObj: { [id: string]: number };
  quotations: any[];
  favoritePairs: string[] = [];
  subs: Subscription[] = [];

  constructor(
    private store: Store,
    public translate: TranslateService,
    private signalR: SignalRService
  ) {}

  toggleOptionsView(event) {
    event.stopPropagation();
    this.showOptions = !this.showOptions;
  }
  ngOnInit() {
    // Init Store by favorite pairs for user
    this.subs.push(
      this.store.select<string[]>(Section.favoritePairs).subscribe(favorite => {
        this.favoritePairs = favorite;
        if (this.quotationsObj) {
          this.updateQuotations(this.quotationsObj);
        }
      })
    );

    this.getQuotations();
  }

  private getQuotations() {
    this.signalR.getQuotations();
    this.subs.push(
      this.signalR.quotations$.subscribe(quotations => {
        this.updateQuotations(quotations);
      })
    );
  }

  private updateQuotations(quotations) {
    // console.log('Updating Pairs Info:::', quotations);

    this.quotationsObj = { ...this.quotationsObj, ...quotations };
    this.quotations = this.pairsInfoAsArray;
  }

  private get pairsInfoAsArray() {
    return Object.keys(this.quotationsObj).map(pairId => {
      const id = pairId.replace('_', ' / ');
      const favorite = this.favoritePairs.includes(pairId);
      return { id, last: this.quotationsObj[pairId], favorite };
    });
  }

  ngOnDestroy() {
    if (this.subs) {
      this.subs.forEach(sub => sub.unsubscribe());
    }
  }

  // get pairsInfoAsArray() {
  //   return Object.keys(this.pairsInfoAsObj).map(pairId => {
  //     const id = pairId.replace('_', ' / ');
  //     const favorite = this.favoritePairs.includes(pairId);
  //     return { id, last: this.pairsInfoAsObj[pairId].last, favorite };
  //   });
  // }

  // private poloniexQuotations() {
  //   this.subs.push(
  //     this.poloniexService
  //       .getTikersFullData()
  //       .pipe(
  //         tap(data => {
  //           this.pairsInfoAsObj = { ...this.pairsInfoAsObj, ...data };
  //           this.pairsInfo = this.pairsInfoAsArray;
  //         }),
  //         switchMap(tickersData => {
  //           this.pairsInfoAsObj = tickersData;
  //           return this.poloniexService.tickerData$;
  //         }),
  //         filter(newPairInfo => {
  //           const id = Object.keys(newPairInfo)[0];
  //           return this.pairsInfoAsObj[id].last !== newPairInfo[id].last;
  //         }),
  //         tap(
  //           data => (this.pairsInfoAsObj = { ...this.pairsInfoAsObj, ...data })
  //         )
  //       )
  //       .subscribe(data => (this.pairsInfo = this.pairsInfoAsArray))
  //   );
  // }
}
