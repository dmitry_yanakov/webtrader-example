import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StringCoinSettingsComponent } from './string-coin-settings.component';

describe('StringCoinSettingsComponent', () => {
  let component: StringCoinSettingsComponent;
  let fixture: ComponentFixture<StringCoinSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StringCoinSettingsComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StringCoinSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
