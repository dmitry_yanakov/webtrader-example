import {
  Component,
  OnInit,
  HostListener,
  ElementRef,
  Output,
  EventEmitter,
  OnDestroy,
  AfterViewInit
} from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Store, Section } from '@app/store';
import { ViewOptions } from '@models';
import { Subscriber, Subscription } from 'rxjs';

@Component({
  selector: 'app-string-coin-settings',
  templateUrl: './string-coin-settings.component.html',
  styleUrls: ['./string-coin-settings.component.scss']
})
export class StringCoinSettingsComponent implements OnInit, OnDestroy {
  formValue: Subscription;
  aboveTheMenu: boolean = this.store.value.viewOptions.aboveTheMenu || false;
  form = new FormGroup({
    favoriteCoins: new FormControl(),
    hotCoins: new FormControl()
    // favoriteAndHotCoins: new FormControl()
  });

  @Output() clickOut = new EventEmitter();
  @HostListener('document:click', ['$event'])
  clickout(event) {
    if (!this.eRef.nativeElement.contains(event.target)) {
      this.clickOut.emit(false);
    }
  }

  constructor(private store: Store, private eRef: ElementRef) {
    // localStorage.removeItem('viewOptions');
  }
  test() {
    this.aboveTheMenu = !this.aboveTheMenu;
    const strOptions = {
      aboveTheMenu: this.aboveTheMenu,
      overChart: !this.aboveTheMenu
    };
    const opt = Object.assign(this.store.value.viewOptions, strOptions);
    this.store.set(Section.viewOptions, opt);
    localStorage.setItem(
      'viewOptions',
      JSON.stringify(this.store.value.viewOptions)
    );
  }
  ngOnInit() {
    const controls = this.form.controls;
    // tslint:disable-next-line:forin
    for (const control in controls) {
      controls[control].setValue(this.store.value.viewOptions[control]);
    }
    this.formValue = this.form.valueChanges.subscribe(res => {
      const opt = Object.assign(this.store.value.viewOptions, res);
      this.store.set(Section.viewOptions, opt);
      localStorage.setItem(
        'viewOptions',
        JSON.stringify(this.store.value.viewOptions)
      );
    });
  }
  ngOnDestroy() {
    this.formValue.unsubscribe();
  }
}
