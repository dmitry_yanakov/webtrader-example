import {
  Component,
  Output,
  Input,
  EventEmitter,
  ViewChild,
  ElementRef
} from '@angular/core';
import { MsgService } from '@services';

@Component({
  selector: 'app-user-messengers',
  templateUrl: './user-messengers.component.html',
  styleUrls: ['./user-messengers.component.scss']
})
export class UserMessengersComponent {
  selectedMessenger: any = {};
  showInput = false;

  @Input() userMessengers: any[];
  @Output() onUpdatedMessengers: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('input') inputMessage: ElementRef;

  constructor(private msgService: MsgService) {}

  showMessengerValue(messenger: any) {
    this.showInput = true;
    this.selectedMessenger = this.userMessengers.find(
      mess => mess.name === messenger.name
    );

    setTimeout(() => {
      <HTMLInputElement>this.inputMessage.nativeElement.focus();
    }, 100);
  }

  updateMessengers(value) {
    if (this.selectedMessenger.value !== value) {
      if (value.length && value.length < 4) {
        this.msgService.showProfileError('MessengerNameMinLength');
        this.showInput = false;
        return;
      }
      this.selectedMessenger.value = value;

      this.onUpdatedMessengers.emit(this.userMessengers);
    }
    this.showInput = false;
  }
}
