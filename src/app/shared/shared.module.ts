import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SquareCircleComponent } from '@components';
import {
  NumberOnlyDirective,
  PhoneNumberDirective,
  OrderNumberDirective,
  DisableControlDirective,
  MoveNextByMaxLengthDirective
} from '@directives';
import { ToastModule } from 'primeng/toast';
import { DropdownModule } from 'primeng/dropdown';
import { QRCodeModule } from 'angularx-qrcode';
import { LoaderComponent } from './components/loader/loader.component';

@NgModule({
  declarations: [
    // Components
    SquareCircleComponent,
    // Directives
    NumberOnlyDirective,
    PhoneNumberDirective,
    OrderNumberDirective,
    DisableControlDirective,
    MoveNextByMaxLengthDirective,
    LoaderComponent
  ],
  imports: [CommonModule, ToastModule, DropdownModule, QRCodeModule],
  exports: [
    // Components
    SquareCircleComponent,
    LoaderComponent,
    // Directives
    NumberOnlyDirective,
    PhoneNumberDirective,
    OrderNumberDirective,
    DisableControlDirective,
    MoveNextByMaxLengthDirective,
    // Shared PrimeNG
    ToastModule,
    DropdownModule,
    QRCodeModule
  ],
  providers: []
})
export class SharedModule {}
