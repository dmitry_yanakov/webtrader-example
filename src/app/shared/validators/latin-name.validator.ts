import { AbstractControl } from '@angular/forms';

export function LatinStringValidator(control: AbstractControl) {
  return /^[a-zA-Z]+([a-zA-Z\s-]+)?[a-zA-Z]$/.test(control.value)
    ? null
    : { invalidLatinString: true };
}

export function LatinAndNumberStringValidator(control: AbstractControl) {
  return /^[a-zA-Z]+([a-zA-Z0-9\s-]+)?[a-zA-Z0-9]+$/.test(control.value)
    ? null
    : { invalidLatinAndNumberString: true };
}
