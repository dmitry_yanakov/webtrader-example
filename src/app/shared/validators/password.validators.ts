import { AbstractControl } from '@angular/forms';

export function PasswordValidator(control: AbstractControl) {
  const value = control.value;

  const hasNumber = /[0-9]/.test(value);
  const hasCapitalLetter = /[A-Z]/.test(value);
  const hasLowercaseLetter = /[a-z]/.test(value);
  const isLengthValid = value ? value.length > 7 : false;
  const passwordValid = hasNumber && hasCapitalLetter && hasLowercaseLetter && isLengthValid;
  if (!passwordValid) {
    return { invalidPassword: 'Password is not valid!' };
  }
  return null;
}

export function PasswordMatchValidator2(control: AbstractControl) {
  return /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{7,}$/.test(control.value)
    ? null
    : { invalidPassword: true };
}

export function PasswordMatchValidator(control: AbstractControl) {
  const password: string = control.get('pass').value; // get password from our password form control
  const confirmPassword: string = control.get('confirmPass').value; // get password from our confirmPassword form control
  // compare is the password math
  if (password !== confirmPassword) {
    // if they don't match, set an error in our confirmPassword form control
    return { NoPasswordMatch: true };
  }
  return null;
}
