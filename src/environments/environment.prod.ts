export const environment = {
  production: true,
  baseUrl: 'http://eqvola-sigma.cloudapp.net:8080/api',
  signalUrl: 'http://eqvola-sigma.cloudapp.net:8082'
};
